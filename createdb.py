from app import app, db

# Create an application context
with app.app_context():
    # Inside the context, indent your code
    db.create_all()

